#include "stdafx.h"
#include "inc/main.h"

#include "script.h"

Ped GetPlayerPed()
{
	if (PLAYER::DOES_MAIN_PLAYER_EXIST())
	{
		return PLAYER::GET_PLAYER_PED(PLAYER::GET_PLAYER_ID());
	}

	return NULL;
}

void main()
{
	Ped worldPeds[64];
	int numPeds = 0;

	int lastGetEntities = 0;

	while (true)
	{
		if (!HUD::IS_PAUSE_MENU_ACTIVE() && CUTSCENE::GET_CUTSCENE_TIME_MS() == 0)
		{
			Ped plrPed = GetPlayerPed();

			if (plrPed != NULL && PED::DOES_PED_EXIST(plrPed) && !PED::IS_PED_DEAD(plrPed))
			{
				int now = MISC::GET_GAME_TIMER();

				if (now >= lastGetEntities + 500)
				{
					numPeds = worldGetAllPeds(worldPeds, 64);

					lastGetEntities = now;
				}

				for (int i = 0; i < numPeds; i++)
				{
					if (worldPeds[i] == plrPed) continue;

					Weapon weapon = WEAPON::GET_WEAPON_FROM_HAND(worldPeds[i], 0, true);

					if (weapon != NULL && WEAPON::GET_DOES_WEAPON_EXIST(weapon))
					{
						int damagedBone;
						PED::GET_PED_LAST_DAMAGE_BONE(worldPeds[i], &damagedBone);

						switch (damagedBone)
						{
						case (int)ePedBone::R_HAND:
						case (int)ePedBone::R_FINGER0:
						case (int)ePedBone::R_FINGER01:
						case (int)ePedBone::R_FINGER02:
						case (int)ePedBone::R_FINGER1:
						case (int)ePedBone::R_FINGER11:
						case (int)ePedBone::R_FINGER12:
						case (int)ePedBone::R_FINGER2:
						case (int)ePedBone::R_FINGER21:
						case (int)ePedBone::R_FINGER22:
						case (int)ePedBone::R_FINGER3:
						case (int)ePedBone::R_FINGER31:
						case (int)ePedBone::R_FINGER32:
						case (int)ePedBone::R_FINGER4:
						case (int)ePedBone::R_FINGER41:
						case (int)ePedBone::R_FINGER42:
						case (int)ePedBone::R_FOREARM:
						case (int)ePedBone::L_HAND:
						case (int)ePedBone::L_FINGER0:
						case (int)ePedBone::L_FINGER01:
						case (int)ePedBone::L_FINGER02:
						case (int)ePedBone::L_FINGER1:
						case (int)ePedBone::L_FINGER11:
						case (int)ePedBone::L_FINGER12:
						case (int)ePedBone::L_FINGER2:
						case (int)ePedBone::L_FINGER21:
						case (int)ePedBone::L_FINGER22:
						case (int)ePedBone::L_FINGER3:
						case (int)ePedBone::L_FINGER31:
						case (int)ePedBone::L_FINGER32:
						case (int)ePedBone::L_FINGER4:
						case (int)ePedBone::L_FINGER41:
						case (int)ePedBone::L_FINGER42:
						case (int)ePedBone::L_FOREARM:
							WEAPON::SET_PED_DROPS_WEAPON(worldPeds[i], weapon);
							PED::CLEAR_PED_LAST_DAMAGE_BONE(worldPeds[i]);
						}
					}

					scriptWait(0);
				}
			}
		}

		scriptWait(0);
	}
}

void ScriptMain()
{
	srand(GetTickCount());
	main();
}
